FROM golang:1.10

ENV ROOT "/go/src/gitlab.com/cleanunicorn/eth-tipper"

WORKDIR $ROOT

ADD Gopkg.lock Gopkg.toml ./
RUN go get -u github.com/golang/dep/cmd/dep
RUN dep ensure -vendor-only

ADD . .
RUN make install

