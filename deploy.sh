#!/bin/bash

DOCKER_IMAGE="registry.gitlab.com/cleanunicorn/eth-tipper:latest"
DISCORD_AUTH="Discord auth token here"
INSTALL_PATH="Where do you have the deploy script and database folder"
# ethereum node to use (must match this with NETWORK_ID)
NODE_URL="https://ropsten.infura.io/VysOhOw6yTJlCUlFBPS3"
# how many blocks to lag behind 
CONFIRMATIONS_REQUIRED="5" 
# database path
DATABASE_PATH="/database/discord.db"

cd $INSTALL_PATH
mkdir database

docker kill eth-tipper
docker rm eth-tipper
docker pull $DOCKER_IMAGE

docker run -d --name=eth-tipper --restart=always -it -e DISCORD_AUTH="$DISCORD_AUTH" -v $INSTALL_PATH/database:/database/ $DOCKER_IMAGE ./eth-tipper discord --node-url "$NODE_URL" --lag-blocks $CONFIRMATIONS_REQUIRED -d $DATABASE_PATH -v
