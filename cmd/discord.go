package cmd

import (
	"os"

	"gitlab.com/cleanunicorn/eth-tipper/core/network"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/cleanunicorn/eth-tipper/core/discord"
	"gitlab.com/cleanunicorn/eth-tipper/core/monitor"
	"gitlab.com/cleanunicorn/eth-tipper/core/types"
)

var (
	nodeURL             string
	lagBlocks           int64
	databasePath        string
	networkDatabasePath string
)

// discord opens a connection to discord and waits for commands
var discordCmd = &cobra.Command{
	Use:   "discord",
	Short: "connect to discord and wait for commands",
	Long:  "connect the bot to discord and wait for the commands",
	Args:  cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		stopChan := make(chan bool, 1)

		// Open database
		log.Debug("Opening database")
		db, err := discord.GetDatabase(databasePath)
		if err != nil {
			log.Fatal("Could not open database ", err)
			return
		}
		log.Debug("Database open")

		log.Debug("Getting central account")
		centralAccount, err := discord.GetCentralAccount(db)
		if err != nil {
			log.Fatal("Could not get central account address ", err)
			return
		}
		log.Debug("Central account retrieved")

		// Start discord bot
		log.Debug("Start discord client")
		var txChan chan types.Transaction = make(chan types.Transaction, 1)
		txPublishChan := make(chan network.TxTransport, 1)
		d := discord.Client{
			AuthenticationToken:    "Bot " + os.Getenv("DISCORD_AUTH"),
			Database:               db,
			TransactionChan:        txChan,
			TransactionPublishChan: txPublishChan,
			CentralAccount:         centralAccount,
		}
		go d.Start(stopChan)
		log.Debug("Discord client started")

		// Start ethereum monitor
		log.Debug("Start ethereum monitor")
		m := monitor.Client{
			NodeURL:         nodeURL,
			Database:        db,
			TransactionChan: txChan,
		}
		go m.Start(lagBlocks)
		log.Debug("Ethereum monitor started")

		log.Debug("Start transaction publisher")
		p := network.TransactionPublisher{
			NodeURL:         nodeURL,
			TransactionChan: txPublishChan,
		}
		go p.Start()
		log.Debug("Transaction publisher started")

		<-stopChan
	},
}

func init() {
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// fetchCmd.PersistentFlags().String("foo", "", "A help for foo")
	// NOTICE: there s a few issues with persistant flags
	// https://github.com/spf13/cobra/search?q=PersistentFlags&type=Issues&utf8=✓

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// create.Flags().StringVarP(&aFlag, "flag", "f", "default-val", "Help message for flag")

	discordCmd.Flags().StringVarP(&nodeURL, "node-url", "n", "http://127.0.0.1:8545", "Link to the ethereum node (parity or geth)")
	discordCmd.Flags().Int64VarP(&lagBlocks, "lag-blocks", "l", 10, "How many blocks to lag when monitoring ethereum")
	discordCmd.Flags().StringVarP(&databasePath, "database", "d", "./discord.db", "What database to use. Will create one if it does not exist.")
	RootCmd.AddCommand(discordCmd)
}
