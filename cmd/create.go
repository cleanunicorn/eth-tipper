package cmd

import (
	"bytes"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/cleanunicorn/eth-tipper/core"
	"gitlab.com/cleanunicorn/eth-tipper/core/network"
)

// create represents a example command
var create = &cobra.Command{
	Use:   "create",
	Short: "create a new wallet",
	Long:  "create a new wallet and return private key",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		a, err := core.CreateWallet()
		if err != nil {
			log.Info("Could not generate wallet", err)
		}
		log.Info("From Address = ", a.Address())
		log.Info("From Private key = ", a.PrivateKey())

		b, err := core.CreateWallet()
		if err != nil {
			log.Info("Could not generate wallet", err)
		}
		log.Info("To Address = ", b.Address())
		log.Info("To Private key = ", b.PrivateKey())

		signer := core.CreateSigner(network.Rinkeby)
		tx, err := core.SignTx(
			signer,
			a,
			0,
			common.HexToAddress(b.Address()),
			big.NewInt(1),
			21000,
			big.NewInt(1),
			[]byte{},
		)

		var buff bytes.Buffer
		tx.EncodeRLP(&buff)
		log.Infof("Signed tx = 0x%x", buff.Bytes())
	},
}

func init() {
	RootCmd.AddCommand(create)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// fetchCmd.PersistentFlags().String("foo", "", "A help for foo")
	// NOTICE: there s a few issues with persistant flags
	// https://github.com/spf13/cobra/search?q=PersistentFlags&type=Issues&utf8=✓

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// create.Flags().StringVarP(&aFlag, "flag", "f", "default-val", "Help message for flag")
}
