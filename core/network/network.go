package network

const Homestead = 1
const Morden = 2
const Ropsten = 3
const Rinkeby = 4
const RootstockMainnet = 30
const RootstockTestnet = 31
const Kovan = 42
const EthereumClassicMainnet = 61
const EthereumClassicTestnet = 62
const GethPrivateChains = 1337
