package network

import (
	"bytes"
	"fmt"
	"math/big"
	"time"

	"gitlab.com/cleanunicorn/eth-tipper/core/ethgasstation"
	"gitlab.com/cleanunicorn/eth-tipper/core/ethunits"

	"github.com/ethereum/go-ethereum/common"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cleanunicorn/eth-tipper/core"
	"gitlab.com/cleanunicorn/eth-tipper/core/jsonrpc"
	"gitlab.com/cleanunicorn/eth-tipper/core/types"
)

type TransactionPublisher struct {
	networkID       int64
	NodeURL         string
	TransactionChan <-chan TxTransport
}

type TxTransport struct {
	Account  types.Account
	To       string
	Amount   *big.Int
	GasLimit uint64
	GasPrice *big.Int
	Data     []byte
}

func (tp *TransactionPublisher) Start() {
	log.Debug("Getting network id of the node")
	node := jsonrpc.CreateServer(tp.NodeURL)
	networkID, err := node.Net_version()
	if err != nil {
		log.Fatal("Could not identify networkID err: ", err)
	}
	tp.networkID = networkID

	log.Debug("Creating signer for network id: ", tp.networkID)
	signer := core.CreateSigner(tp.networkID)

	log.Debug("Pick gas price before starting to process transactions")
	gp, err := ethgasstation.GetPrices()
	if err != nil {
		log.Fatal("Could not get price from ethgasstation err: ", err)
	}
	gasPriceWei, err := ethunits.ToWei(fmt.Sprintf("%g", gp.SafeLow/10), "gwei")
	if err != nil {
		log.Fatalf("Could not transform eth gas station price to wei err: %s, gasPrice: %g", err, gp.SafeLow)
	}
	gasPrice, ok := big.NewInt(0).SetString(gasPriceWei, 10)
	if ok != true {
		log.Fatalf("Could not set gasPrice")
	}
	log.Debug("Gas price = ", gasPrice)

	// Update gasPrice every 10 minutes
	go func() {
		for {
			time.Sleep(1 * time.Minute)
			gp, err := ethgasstation.GetPrices()
			if err != nil {
				log.Error("Could not get price from ethgasstation err: ", err)
				continue
			}
			gasPriceWei, err := ethunits.ToWei(fmt.Sprintf("%g", gp.SafeLow/10), "gwei")
			if err != nil {
				log.Errorf("Could not transform eth gas station price to wei err: %s, gasPrice: %g", err, gp.SafeLow)
				continue
			}
			gasPrice, ok = big.NewInt(0).SetString(gasPriceWei, 10)
			if ok != true {
				log.Error("Could not set gasPriceWei: ", gasPriceWei)
				continue
			}
			log.Debugf("Gas price = %v gwei", gp.SafeLow/10)
		}
	}()

	for tx := range tp.TransactionChan {
		nonce, err := node.Eth_getTransactionCount(tx.Account.Address(), "latest")
		if err != nil {
			log.Error("Could not get nonce for account: ", tx.Account.Address(), " err: ", err)
			continue
		}

		txFee := new(big.Int).Mul(big.NewInt(21000), gasPrice)
		tx.Amount.Sub(tx.Amount, txFee)

		tx, err := core.SignTx(
			signer,
			tx.Account,
			nonce,
			common.HexToAddress(tx.To),
			tx.Amount,
			21000,
			gasPrice,
			[]byte{},
		)
		if err != nil {
			log.Error("Could not sign transaction", err)
			continue
		}

		var txBuff bytes.Buffer
		tx.EncodeRLP(&txBuff)

		txHash, err := node.Eth_sendRawTransaction(fmt.Sprintf("0x%x", txBuff.Bytes()))
		if err != nil {
			log.Errorf("Error publishing transaction: %#v, err: %s", tx, err)
			continue
		}
		log.Debugf("Published transaction hash: %s", txHash)
	}
}
