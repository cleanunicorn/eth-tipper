package core_test

import (
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/cleanunicorn/eth-tipper/core"
	"gitlab.com/cleanunicorn/eth-tipper/core/network"
)

func TestSignTx(t *testing.T) {
	a, _ := core.CreateWallet()
	b, _ := core.CreateWallet()

	var chainID int64 = network.Rinkeby
	signer := core.CreateSigner(chainID)

	txSigned, err := core.SignTx(
		// signer
		signer,
		// account
		a,
		// nonce
		uint64(3),
		// to
		common.HexToAddress(b.Address()),
		// amount
		big.NewInt(1),
		// gasLimit
		uint64(21000),
		// gasPrice
		big.NewInt(2),
		// data
		[]byte{},
	)
	if err != nil {
		t.Error("Could not sign tx", err)
	}

	if big.NewInt(21000).Cmp(big.NewInt(int64(txSigned.Gas()))) != 0 {
		t.Error("Signed tx with incorrect gas limit \texpected:", big.NewInt(21000), "\tgot:", txSigned.Gas())
	}

	if big.NewInt(2).Cmp(txSigned.GasPrice()) != 0 {
		t.Error("Signed tx with incorrect gas price \texpected:", big.NewInt(2), "\tgot:", txSigned.GasPrice())
	}

	if big.NewInt(1).Cmp(txSigned.Value()) != 0 {
		t.Error("Signed tx with incorrect value sent \texpected:", big.NewInt(1), "\tgot:", txSigned.Value())
	}

	if txSigned.To().Hex() != b.Address() {
		t.Error("Signed tx with incorrect to \texpected:", b.Address(), "\tgot:", txSigned.To().Hex())
	}

	if txSigned.ChainId().Cmp(big.NewInt(chainID)) != 0 {
		t.Error("Chain ID incorrect \texpected:", chainID, "\tgot:", txSigned.ChainId())
	}

	// amount + gasprice * gaslimit
	if big.NewInt(42001).Cmp(txSigned.Cost()) != 0 {
		t.Error("Signed tx with incorrect total transaction cost \texpected:", big.NewInt(21001), "\tgot:", txSigned.Cost())
	}
}
