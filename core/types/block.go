package types

type RPCBlockResponse struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  struct {
		Author           string        `json:"author"`
		Difficulty       string        `json:"difficulty"`
		ExtraData        string        `json:"extraData"`
		GasLimit         string        `json:"gasLimit"`
		GasUsed          string        `json:"gasUsed"`
		Hash             string        `json:"hash"`
		LogsBloom        string        `json:"logsBloom"`
		Miner            string        `json:"miner"`
		Number           string        `json:"number"`
		ParentHash       string        `json:"parentHash"`
		ReceiptsRoot     string        `json:"receiptsRoot"`
		SealFields       []string      `json:"sealFields"`
		Sha3Uncles       string        `json:"sha3Uncles"`
		Signature        string        `json:"signature"`
		Size             string        `json:"size"`
		StateRoot        string        `json:"stateRoot"`
		Step             string        `json:"step"`
		Timestamp        string        `json:"timestamp"`
		TotalDifficulty  string        `json:"totalDifficulty"`
		Transactions     []Transaction `json:"transactions"`
		TransactionsRoot string        `json:"transactionsRoot"`
		Uncles           []interface{} `json:"uncles"`
	} `json:"result"`
	ID int `json:"id"`
}

type Transaction struct {
	BlockHash        string      `json:"blockHash"`
	BlockNumber      string      `json:"blockNumber"`
	ChainID          interface{} `json:"chainId"`
	Condition        interface{} `json:"condition"`
	Creates          interface{} `json:"creates"`
	From             string      `json:"from"`
	Gas              string      `json:"gas"`
	GasPrice         string      `json:"gasPrice"`
	Hash             string      `json:"hash"`
	Input            string      `json:"input"`
	Nonce            string      `json:"nonce"`
	PublicKey        string      `json:"publicKey"`
	R                string      `json:"r"`
	Raw              string      `json:"raw"`
	S                string      `json:"s"`
	StandardV        string      `json:"standardV"`
	To               string      `json:"to"`
	TransactionIndex string      `json:"transactionIndex"`
	V                string      `json:"v"`
	Value            string      `json:"value"`
}

type RPCBlockRequest struct {
	Jsonrpc string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
	ID      int           `json:"id"`
}
