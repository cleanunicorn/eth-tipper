package jsonrpc

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

type JSONRPCEthereumServer struct {
	url string
}

func CreateServer(url string) *JSONRPCEthereumServer {
	return &JSONRPCEthereumServer{
		url: url,
	}
}

func (s *JSONRPCEthereumServer) makeCall(method string, args []string) ([]byte, error) {
	data := map[string]interface{}{
		"jsonrpc": "2.0",
		"method":  method,
		"params":  args,
		"id":      1,
	}
	dataJSON, err := json.Marshal(data)
	if err != nil {
		return []byte{}, err
	}

	log.Debug("Making request ", string(dataJSON))

	request, err := http.NewRequest("POST", s.url, strings.NewReader(string(dataJSON)))
	if err != nil {
		return []byte{}, err
	}
	defer request.Body.Close()

	request.Header.Add("Content-Type", "application/json")

	var httpTransport = &http.Transport{}
	var httpClient = &http.Client{Transport: httpTransport}

	response, err := httpClient.Do(request)
	if err != nil {
		return []byte{}, err
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return []byte{}, err
	}
	defer response.Body.Close()

	return body, nil
}

func (s *JSONRPCEthereumServer) Eth_getTransactionCount(account string, block string) (uint64, error) {
	reply, err := s.makeCall("eth_getTransactionCount", []string{account, block})
	if err != nil {
		return 0, err
	}

	type eth_getTransactionCount struct {
		Jsonrpc string `json:"jsonrpc"`
		Result  string `json:"result"`
		ID      uint   `json:"id"`
	}
	var transactionCountReply eth_getTransactionCount

	err = json.Unmarshal(reply, &transactionCountReply)
	if err != nil {
		return 0, err
	}

	count, err := strconv.ParseUint(transactionCountReply.Result, 0, 64)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (s *JSONRPCEthereumServer) Eth_getBalance(account string, block string) (*big.Int, error) {
	reply, err := s.makeCall("eth_getBalance", []string{account, block})
	if err != nil {
		return big.NewInt(0), err
	}

	type eth_getBalance struct {
		Jsonrpc string `json:"jsonrpc"`
		Result  string `json:"result"`
		ID      uint   `json:"id"`
	}
	var balanceReply eth_getBalance

	err = json.Unmarshal(reply, &balanceReply)
	if err != nil {
		return big.NewInt(0), err
	}

	balance := hexStrToBigInt(balanceReply.Result)

	return balance, nil
}

func (s *JSONRPCEthereumServer) Eth_sendRawTransaction(signedTransaction string) (string, error) {
	reply, err := s.makeCall("eth_sendRawTransaction", []string{signedTransaction})
	if err != nil {
		return "", err
	}

	type eth_sendRawTransaction struct {
		Jsonrpc string `json:"jsonrpc"`
		Result  string `json:"result"`
		ID      uint   `json:"id"`
	}
	var transactionHashReply eth_sendRawTransaction

	err = json.Unmarshal(reply, &transactionHashReply)
	if err != nil {
		return "", err
	}

	if strings.Compare(transactionHashReply.Result, "") == 0 {
		type eth_sendRawTransactionError struct {
			Jsonrpc string `json:"jsonrpc"`
			Error   struct {
				Code    int    `json:"code"`
				Message string `json:"message"`
			} `json:"error"`
			ID int `json:"id"`
		}

		var transactionError eth_sendRawTransactionError
		err := json.Unmarshal(reply, &transactionError)
		if err != nil {
			return "", err
		}

		if transactionError.Error.Code == 0 {
			return "", fmt.Errorf("unknown error, got reply: %s", string(reply))
		}

		return "", fmt.Errorf("no transaction hash generated, got error code: %d message: %s", transactionError.Error.Code, transactionError.Error.Message)
	}

	return transactionHashReply.Result, nil
}

// Net_version returns the current network id
// https://github.com/ethereum/wiki/wiki/JSON-RPC#net_version
func (s *JSONRPCEthereumServer) Net_version() (int64, error) {
	reply, err := s.makeCall("net_version", []string{})
	if err != nil {
		return 0, err
	}

	type net_version struct {
		ID      int    `json:"id"`
		Jsonrpc string `json:"jsonrpc"`
		Result  int64  `json:"result,string"`
	}
	var networkIDReply net_version

	err = json.Unmarshal(reply, &networkIDReply)
	if err != nil {
		return 0, err
	}

	return networkIDReply.Result, nil
}

func (s *JSONRPCEthereumServer) eth_blockNumber() (*big.Int, error) {
	reply, err := s.makeCall("eth_blockNumber", []string{})
	if err != nil {
		return big.NewInt(0), err
	}

	type eth_blockNumber struct {
		Jsonrpc string `json:"jsonrpc"`
		Result  string `json:"result"`
		ID      uint   `json:"id"`
	}
	var blockNumberReply eth_blockNumber

	err = json.Unmarshal(reply, &blockNumberReply)
	if err != nil {
		return big.NewInt(0), err
	}

	blockNumber := hexStrToBigInt(blockNumberReply.Result)

	return blockNumber, nil
}

func trim0x(PrefixedString string) string {
	return strings.TrimPrefix(PrefixedString, "0x")
}

func hexStrToBigInt(HexString string) *big.Int {
	value := new(big.Int)
	value.SetString(trim0x(HexString), 16)
	return value
}
