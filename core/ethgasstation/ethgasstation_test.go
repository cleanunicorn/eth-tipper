package ethgasstation

import (
	"testing"
)

func TestGetPrices(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name:    "Get gas prices",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := GetPrices()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetPrices() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
