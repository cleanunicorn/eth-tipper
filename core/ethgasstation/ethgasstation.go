package ethgasstation

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
)

const ethgasAPI = "http://ethgasstation.info/json/ethgasAPI.json"

type Prices struct {
	Average       float64 `json:"average"`
	AverageCalc   float64 `json:"average_calc"`
	SafelowCalc   float64 `json:"safelow_calc"`
	BlockNum      int     `json:"blockNum"`
	SafeLow       float64 `json:"safeLow"`
	SafeLowWait   float64 `json:"safeLowWait"`
	Fastest       float64 `json:"fastest"`
	Speed         float64 `json:"speed"`
	FastestWait   float64 `json:"fastestWait"`
	FastWait      float64 `json:"fastWait"`
	BlockTime     float64 `json:"block_time"`
	AvgWait       float64 `json:"avgWait"`
	SafelowTxpool float64 `json:"safelow_txpool"`
	AverageTxpool float64 `json:"average_txpool"`
	Fast          float64 `json:"fast"`
}

func GetPrices() (Prices, error) {
	var p Prices
	err := getURL(ethgasAPI, &p)
	if err != nil {
		log.Debug("Error getting prices")
		return Prices{}, err
	}

	return p, nil
}

func getURL(url string, prices *Prices) error {
	log.Debug("Getting gas prices")

	response, err := http.Get(url)
	if err != nil {
		return err
	}
	pricesData, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		return err
	}

	err = json.Unmarshal(pricesData, &prices)
	if err != nil {
		return err
	}

	return nil
}
