package discord

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

func (d *Client) commandHelp(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Do not trigger yourself
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.Compare(m.Content, d.prefixRegexString()+" help") == 0 {
		// @midas help
		s.ChannelMessageSend(
			m.ChannelID,
			"\n"+
				"**"+d.prefixRegexString()+" help** display help\n"+
				"**"+d.prefixRegexString()+" balance** display your account's balance\n"+
				"**"+d.prefixRegexString()+" deposit** create and display your account address\n"+
				"**"+d.prefixRegexString()+" send @username 1 eth** send *1 eth* to *@username*\n"+
				"**"+d.prefixRegexString()+" withdraw [0xaccountAddress] [value] eth** withdraw your account's balance into an account you have access to\n"+
				"**"+d.prefixRegexString()+" withdraw [0xaccountAddress]** withdraw full account's balance into an account you have access to\n",
		)
	}
}
