package discord

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cleanunicorn/eth-tipper/core/ethunits"
)

func (d *Client) commandBalance(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Do not trigger yourself
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.Compare(m.Content, d.prefixRegexString()+" balance") == 0 {
		b, err := d.getBalanceForUser(m.Author)
		if err != nil {
			log.Error("Error getting balance", err)
			_, err := s.ChannelMessageSend(m.ChannelID, fmt.Sprint("There was an error getting the wallet for user ", m.Author.Username, "err: ", err))
			if err != nil {
				log.Error("Error sending message", err)
				return
			}
			return
		}

		ethBalance, err := ethunits.ToEth(b.String(), "wei")
		if err != nil {
			log.Error("Cannot transform balance to eth err: ", err)
		}
		_, err = s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Your balance is %s ETH", ethBalance))
	}
}
