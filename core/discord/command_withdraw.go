package discord

import (
	"fmt"
	"math/big"
	"regexp"
	"strings"

	"github.com/bwmarrin/discordgo"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cleanunicorn/eth-tipper/core/ethunits"
	"gitlab.com/cleanunicorn/eth-tipper/core/network"
)

func (d *Client) commandWithdraw(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Do not trigger yourself
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.HasPrefix(m.Content, d.prefixRegexString()+" withdraw") {
		withdrawAddressValue := regexp.MustCompile(d.prefixRegexString() + " withdraw ((0x)?[0-9a-fA-F]{40}) (([0-9]*[.])?[0-9]+) (.*?)$")
		withdrawAddressAll := regexp.MustCompile(d.prefixRegexString() + " withdraw ((0x)?[0-9a-fA-F]{40})$")

		var addr string
		var value = big.NewInt(0)

		userBalance, err := d.getBalanceForUser(m.Author)
		if userBalance.Cmp(big.NewInt(0)) <= 0 {
			s.ChannelMessageSend(m.ChannelID, "You have 0 ETH in your account, go help some people!")
			return
		}

		if withdrawAddressValue.MatchString(m.Content) {
			matches := withdrawAddressValue.FindAllStringSubmatch(m.Content, -1)
			if len(matches[0]) != 6 {
				log.Error("Command incorrect matches length expecting: 4\tgot: ", len(matches), "\tmatches: ", matches)
				s.ChannelMessageSend(m.ChannelID, strings.Join(matches[0], "Could not understand command"))
				return
			}

			addr = matches[0][1]
			val, err := ethunits.ToWei(matches[0][3], matches[0][5])
			if err != nil {
				log.Errorf("Could not transform value: %s, unit: %s to wei, err: %s", matches[0][3], matches[0][5], err)
				s.ChannelMessageSend(m.ChannelID, "Could not understand ether value and unit")
				return
			}

			var ok bool
			value, ok = big.NewInt(0).SetString(val, 10)
			if ok != true {
				log.Error("Error transforming ", val, " to Int")
				s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Error transforming %s %s to Wei", matches[0][3], matches[0][5]))
				return
			}

		} else if withdrawAddressAll.MatchString(m.Content) {
			matches := withdrawAddressAll.FindAllStringSubmatch(m.Content, -1)
			if len(matches[0]) != 3 {
				log.Error("Command incorrect matches length expecting: 3\tgot: ", len(matches), "\tmatches: ", matches)
				s.ChannelMessageSend(m.ChannelID, strings.Join(matches[0], "Could not understand command"))
				return
			}

			addr = matches[0][1]

			if err != nil {
				log.Error("Error setting max withdraw value for ID ", m.Author.ID, " err ", err)
				return
			}
			value = userBalance
		}

		if len(addr) == 0 {
			log.Warning("Could not match address to withdraw funds to")
			s.ChannelMessageSend(m.ChannelID, "Could not understand where to send the funds")
			return
		}

		if userBalance.Cmp(value) == -1 {
			userBalanceEth, err := ethunits.ToEth(userBalance.String(), "wei")
			if err != nil {
				log.Error("Cannot transform userBalance to eth, err: ", err)
			}
			valueEth, err := ethunits.ToEth(value.String(), "wei")
			if err != nil {
				log.Error("Cannot transform value to eth, err: ", err)
			}

			log.Warningf("User tried to withdraw more than he has, user balance: %s, tried to withdraw: %s", userBalance.String(), value.String())
			s.ChannelMessageSend(
				m.ChannelID,
				fmt.Sprintf(
					"Cannot withdraw more than you have, your balance: %s ETH, tried to withdraw: %s ETH",
					userBalanceEth,
					valueEth,
				),
			)
			return
		}

		// Check if value withdrawn is less than the minimum allowed value
		// This is important because there needs to be enough ETH to pay for the fee
		withdrawMinLimit := "0.0001"
		withdrawMin, err := ethunits.ToWei("0.0001", "eth")
		if err != nil {
			log.Error("Could not set min withdraw value err: ", err)
			s.ChannelMessageSend(m.ChannelID, "Minimum withdraw value was not understood, internal error")
			return
		}
		withdrawMinValue, _ := big.NewInt(0).SetString(withdrawMin, 10)
		if value.Cmp(withdrawMinValue) == -1 {
			log.Infof("User tried to withdraw less than the defined minimum value: %s ETH, value: %s ", withdrawMinLimit)
			s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("You cannot withdraw less than %s ETH", withdrawMinLimit))
			return
		}

		centralAccount, err := GetCentralAccount(d.Database)
		if err != nil {
			log.Error("Could not get central account, err: ", err)
			return
		}

		// Decrease user value
		err = d.subBalanceFromUser(m.Author, value)
		if err != nil {
			log.Errorf("Could not decrease user balance, err: %s", err)
			s.ChannelMessageSend(m.ChannelID, "Could not decrease your balance by the value requested")
			return
		}

		// Send transaction
		d.TransactionPublishChan <- network.TxTransport{
			Account: centralAccount,
			To:      addr,
			Amount:  value,
		}

		valueWei, err := ethunits.ToEth(value.String(), "wei")
		if err != nil {
			log.Errorf("Could not transform: %s to wei, err: %s", value.String(), err)
			s.ChannelMessageSend(m.ChannelID, "Could not understand value to send in wei")
			return
		}
		s.ChannelMessageSend(
			m.ChannelID,
			fmt.Sprintf("You will receive: %s ETH at address: %s",
				valueWei,
				addr,
			),
		)
	}
}
