package discord

import (
	"encoding/json"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	bolt "github.com/coreos/bbolt"
	"github.com/ethereum/go-ethereum/crypto"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cleanunicorn/eth-tipper/core"
	"gitlab.com/cleanunicorn/eth-tipper/core/types"
)

var (
	bucketUsers          = []byte("users")
	bucketCentralAccount = []byte("centralAccount")
)

type user struct {
	ID               string
	Username         string
	WalletPrivateKey string
	WalletAddress    string
	Balance          *big.Int
}

type centralAccount struct {
	WalletPrivateKey string
	WalletAddress    string
}

// GetDatabase - Open and return database
func GetDatabase(databasePath string) (*bolt.DB, error) {
	db, err := bolt.Open(databasePath, 0600, &bolt.Options{
		Timeout: 1 * time.Second,
	})
	if err != nil {
		log.Error("Could not open database", err)
		return nil, err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists([]byte(bucketUsers))
		tx.CreateBucketIfNotExists([]byte(bucketCentralAccount))
		return nil
	})
	if err != nil {
		log.Error("Could not create buckets", err)
		return nil, err
	}

	return db, nil
}

// GetCentralAccount retrieves the bot's central account
func GetCentralAccount(db *bolt.DB) (types.Account, error) {
	var acc types.Account

	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketCentralAccount)
		ca := b.Get([]byte("0"))
		var a centralAccount

		// If central account exists return it
		if ca != nil {
			err := json.Unmarshal(ca, &a)
			if err == nil {
				pk, err := crypto.HexToECDSA(a.WalletPrivateKey)
				if err == nil {
					acc = types.Account{
						Key: *pk,
					}
					return nil
				}
				log.Error("Could not transform hex private key to ECDSA ", err)
				return err
			}
			log.Error("Could not unmarshal central account ", err)
			return err
		}

		// If it doesn't exist create one
		wallet, err := core.CreateWallet()
		if err != nil {
			log.Error("Error creating central wallet", err)
			return err
		}

		a.WalletAddress = wallet.Address()
		a.WalletPrivateKey = wallet.PrivateKey()
		aBytes, err := json.Marshal(a)
		if err != nil {
			return err
		}

		b.Put([]byte("0"), aBytes)

		acc = types.Account{
			Key: wallet.Key,
		}

		return nil
	})
	if err != nil {
		log.Error("Could not retrieve central account address ", err)
		return types.Account{}, err
	}

	return acc, nil
}

// Get the available wallet for the user or create a new one
func (d *Client) getWalletForUser(author *discordgo.User) (string, error) {
	var u user

	err := d.Database.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketUsers)
		uf := b.Get([]byte(author.ID))

		// User found
		if uf != nil {
			return json.Unmarshal(uf, &u)
		}

		// Create and save the user
		wallet, err := core.CreateWallet()
		if err != nil {
			log.Error("Error creating wallet", err)
			return err
		}

		u.ID = author.ID
		u.Username = author.Username
		u.WalletPrivateKey = wallet.PrivateKey()
		u.WalletAddress = wallet.Address()
		u.Balance = big.NewInt(0)
		uBytes, err := json.Marshal(u)
		if err != nil {
			return err
		}
		b.Put([]byte(author.ID), uBytes)

		return nil
	})
	if err != nil {
		log.Error("Could not retrieve wallet for ID ", author.ID, " username ", author.Username)
		return "", err
	}

	return u.WalletAddress, nil
}

func (d *Client) getBalanceForUser(author *discordgo.User) (*big.Int, error) {
	var balance *big.Int

	err := d.Database.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketUsers)
		uf := b.Get([]byte(author.ID))
		var u user

		if uf == nil {
			return fmt.Errorf("Could not find user %s to get balance", author.ID)
		}

		if err := json.Unmarshal(uf, &u); err != nil {
			return err
		}

		balance = u.Balance

		return nil
	})
	if err != nil {
		log.Error("Could not retrieve balance for ID ", author.ID, " username ", author.Username)
		return nil, err
	}

	return balance, nil
}

func (d *Client) addBalanceToUser(author *discordgo.User, value *big.Int) error {
	balance, err := d.getBalanceForUser(author)
	if err != nil {
		log.Error("Could not get balance for user: ", author.ID, " err: ", err)
		return err
	}

	balance.Add(balance, value)

	if err := d.setBalanceForUser(author, balance); err != nil {
		log.Error("Could not set balance for user: ", author.ID, " err: ", err)
		return err
	}

	return nil
}

func (d *Client) subBalanceFromUser(author *discordgo.User, value *big.Int) error {
	balance, err := d.getBalanceForUser(author)
	if err != nil {
		log.Error("Could not get balance for user: ", author.ID, " err: ", err)
		return err
	}

	balance.Sub(balance, value)

	if err := d.setBalanceForUser(author, balance); err != nil {
		log.Error("Could not set balance for user: ", author.ID, " err: ", err)
	}

	return nil
}

func (d *Client) setBalanceForUser(author *discordgo.User, value *big.Int) error {
	err := d.Database.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketUsers)
		uf := b.Get([]byte(author.ID))
		if uf == nil {
			log.Error("Could not get balance for user ", author.ID, " not found")
			return fmt.Errorf("Could not set balance for user %s, not found ", author.ID)
		}

		var u user
		if err := json.Unmarshal(uf, &u); err != nil {
			log.Error("Could not unmarshal user ", string(uf), " err ", err)
			return err
		}

		u.Balance = value

		uBytes, err := json.Marshal(u)
		if err != nil {
			log.Error("Error marshalling user data for user ", string(uf), " err ", err)
			return err
		}

		b.Put([]byte(author.ID), uBytes)

		return nil
	})

	return err
}

func (d *Client) getIDForUser(username string) (string, error) {
	var userID string

	err := d.Database.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketUsers)
		b.ForEach(func(k []byte, v []byte) error {
			var u user

			err := json.Unmarshal(v, &u)
			if err != nil {
				log.Error("Could not unmarshal user data err: ", err)
				return err
			}

			if strings.EqualFold(u.Username, username) {
				userID = u.ID
				return nil
			}

			return nil
		})
		if len(userID) == 0 {
			log.Errorf("Did not find user with username: %s", username)
			return fmt.Errorf("Did not find user with username: %s", username)
		}

		return nil
	})
	if err != nil {
		log.Error("Could not get ID for username: ", username)
		return "", err
	}

	return userID, nil
}

func (d *Client) getUsers() ([]user, error) {
	var users []user

	err := d.Database.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketUsers)
		err := b.ForEach(func(k []byte, v []byte) error {
			var u user

			err := json.Unmarshal(v, &u)
			if err != nil {
				log.Error("Could not unmarshal user data err: ", err)
				return err
			}

			users = append(users, u)

			return nil
		})
		if err != nil {
			log.Error("Could not get retrieve all users data from database ", err)
			return err
		}

		return nil
	})
	if err != nil {
		log.Error("Could not get all users ", err)
		return []user{}, err
	}

	return users, nil
}
