package discord

import (
	"fmt"
	"math/big"
	"regexp"
	"strings"

	"gitlab.com/cleanunicorn/eth-tipper/core/ethunits"

	"github.com/bwmarrin/discordgo"
	log "github.com/sirupsen/logrus"
)

func (d *Client) commandSend(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Do not trigger yourself
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.HasPrefix(m.Content, d.prefixRegexString()+" send") {
		tipUsername := regexp.MustCompile(d.prefixRegexString() + ` send\s*<@!*([0-9]*?)>\s*(([0-9]*[.])?[0-9]+)\s*(.*?)$`)
		matches := tipUsername.FindAllStringSubmatch(m.Content, -1)

		if len(matches) == 0 {
			log.Warning("Could not match command for sending funds message: ", m.Content)
			s.ChannelMessageSend(m.ChannelID, "Could not understand who to send to and how much")
			return
		}

		userID := matches[0][1]
		if userID == "" {
			log.Info("Could not match userID ", matches)
			s.ChannelMessageSend(m.ChannelID, "Could not understand what user to send to, use @username and autocomplete feature.")
			return
		}
		receiver, err := s.User(userID)
		if err != nil {
			log.Warning("Could not find user info to send err: ", err)
			s.ChannelMessageSend(m.ChannelID, "Could not find user in discord to send funds to. Did you use autocomplete for username?")
			return
		}

		// Match value sent
		v, err := ethunits.ToWei(matches[0][2], matches[0][4])
		if err != nil {
			log.Warningf("Could not transform '%s %s' to wei", matches[0][2], matches[0][4])
			s.ChannelMessageSend(m.ChannelID, "Could not understand amount to send to user.")
			return
		}
		value, ok := new(big.Int).SetString(v, 10)
		if ok != true {
			log.Errorf("Could not transform %s to big.Int", v)
			s.ChannelMessageSend(m.ChannelID, "Could not understand amount")
			return
		}

		// Check balance and tip user
		balance, err := d.getBalanceForUser(m.Author)
		if err != nil {
			log.Error("Could not get user balance")
			return
		}
		balanceEth, err := ethunits.ToEth(balance.String(), "wei")
		valueEth, err := ethunits.ToEth(value.String(), "wei")
		if balance.Cmp(value) == -1 {
			log.Warning("Not enough balance to send")
			s.ChannelMessageSend(
				m.ChannelID,
				fmt.Sprintf(
					"You don't have enough balance to send. You have %s ETH and tried to send %s ETH",
					balanceEth,
					valueEth,
				),
			)
			return
		}

		// Substract from sender
		err = d.subBalanceFromUser(m.Author, value)
		if err != nil {
			log.Error("Could not substract balance for user: ", m.Author, " err: ", err)
			return
		}
		// Force creation of user wallet if there is none
		// TODO: smarter call here instead of always forcing the wallet creation
		_, err = d.getWalletForUser(receiver)
		if err != nil {
			log.Errorf("Could not get or create wallet for user, receiver: %#v", receiver)
			s.ChannelMessageSend(m.ChannelID, "Error sending to user.")
			return
		}
		// Update receiver balance
		err = d.addBalanceToUser(
			&discordgo.User{
				ID:       receiver.ID,
				Username: receiver.Username,
			},
			value,
		)
		if err != nil {
			log.Error("Could not increase balance for user err: ", err)
			return
		}

		// Send message confirmation to sender
		if err != nil {
			log.Error("Could not transform value to ETH err: ", err)
			return
		}
		s.ChannelMessageSend(
			m.ChannelID,
			fmt.Sprintf(
				"Sent %s ETH to <@!%s>",
				valueEth,
				receiver.ID,
			),
		)

		// Send message confirmation to receiver
		receiverChannel, err := s.UserChannelCreate(receiver.ID)
		if err != nil {
			log.Error("Error starting channel with receiver err: ", err)
			return
		}
		s.ChannelMessageSend(
			receiverChannel.ID,
			fmt.Sprintf(
				"Received %s ETH from %s",
				valueEth,
				m.Author.Username,
			),
		)

		return
	}
}
