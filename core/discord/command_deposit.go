package discord

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	log "github.com/sirupsen/logrus"
)

func (d *Client) commandDeposit(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Do not trigger yourself
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.Compare(m.Content, d.prefixRegexString()+" deposit") == 0 {
		w, err := d.getWalletForUser(m.Author)
		if err != nil {
			_, err := s.ChannelMessageSend(m.ChannelID, fmt.Sprint("There was an error getting the wallet for user ", m.Author.Username, " err: ", err))
			if err != nil {
				log.Error("Error sending message", err)
				return
			}
			return
		}

		_, err = s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Howdy %s! You can send ETH to this wallet %s", m.Author.Username, w))
		if err != nil {
			log.Error("Error sending message", err)
			return
		}
	}
}
