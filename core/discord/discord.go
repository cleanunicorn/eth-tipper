package discord

import (
	"fmt"
	"math/big"
	"strings"

	"github.com/bwmarrin/discordgo"
	bolt "github.com/coreos/bbolt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cleanunicorn/eth-tipper/core/ethunits"
	"gitlab.com/cleanunicorn/eth-tipper/core/network"
	"gitlab.com/cleanunicorn/eth-tipper/core/types"
)

// Client wrapper over discord bot with token, database, transaction channels,...
type Client struct {
	AuthenticationToken    string
	Database               *bolt.DB
	TransactionChan        <-chan types.Transaction
	CentralAccount         types.Account
	TransactionPublishChan chan<- network.TxTransport
	Session                *discordgo.Session
	BotUser                *discordgo.User
}

// Start discord bot
func (d *Client) Start(stopChan chan bool) {
	// Connect to bot discord
	var err error
	d.Session, err = discordgo.New(d.AuthenticationToken)
	if err != nil {
		log.Fatal("Could not init bot", err)
		return
	}

	log.Debug("Getting bot user")
	me, err := d.Session.User("@me")
	if err != nil {
		log.Fatal("Could not get bot user struct err: ", err)
	}
	d.BotUser = me
	log.Debug("Got bot user")

	log.Debug("Adding commands")
	d.Session.AddHandler(d.commandDeposit)
	d.Session.AddHandler(d.commandBalance)
	d.Session.AddHandler(d.commandWithdraw)
	d.Session.AddHandler(d.commandSend)
	d.Session.AddHandler(d.commandHelp)
	log.Debug("Done adding commands")

	log.Debug("Starting bot")
	err = d.Session.Open()
	if err != nil {
		log.Fatal("Could not start bot err: ", err)
		return
	}
	log.Debug("Bot started")
	defer d.Session.Close()

	for tx := range d.TransactionChan {
		d.checkUsersDeposit(tx)
	}

	log.Debug("Waiting for close")
	<-stopChan
	log.Debug("")

	return
}

func (d *Client) checkUsersDeposit(tx types.Transaction) error {
	users, err := d.getUsers()
	if err != nil {
		log.Error("Could not check if user received funds because user list was not returned")
	}

	for _, u := range users {
		// Transaction entered the user's gateway wallet
		// This creates another transaction to the centralized bot wallet.
		// No balance is recorded yet
		if strings.ToLower(u.WalletAddress) == strings.ToLower(tx.To) {
			value := new(big.Int)
			value, ok := value.SetString(tx.Value, 0)
			if ok != true {
				log.Error("Could not transform hex ", tx.Value, " to int")
				return fmt.Errorf("Could not transform hex to int")
			}

			a, err := types.AccountFromHexKey(u.WalletPrivateKey)
			if err != nil {
				log.Error()
				return fmt.Errorf("Could not generate account from private key")
			}

			d.TransactionPublishChan <- network.TxTransport{
				Account: a,
				To:      d.CentralAccount.Address(),
				Amount:  value,
			}

			log.Info("User's wallet received deposit user: ", u.Username, " amount: ", value)
		}

		// Transaction from user's gateway wallet to centralized wallet
		// This increases the user's balance
		if strings.ToLower(u.WalletAddress) == strings.ToLower(tx.From) {
			value := new(big.Int)
			value, ok := value.SetString(tx.Value, 0)
			if ok != true {
				log.Error("Could not transform hex ", tx.Value, " to int")
				return fmt.Errorf("Could not transform hex to int")
			}

			err := d.addBalanceToUser(
				&discordgo.User{
					ID: u.ID,
				},
				value,
			)
			if err != nil {
				log.Error("Could not increase balance for user: ", u.Username, " value: ", value.String())
				continue
			}

			// Send message to user telling him deposit was confirmed
			balance, err := d.getBalanceForUser(&discordgo.User{
				ID: u.ID,
			})
			uChannel, err := d.Session.UserChannelCreate(u.ID)
			if err != nil {
				log.Error("Error starting channel with wallet owner err: ", err)
				continue
			}

			valueEth, err := ethunits.ToEth(value.String(), "wei")
			if err != nil {
				log.Error("Error transforming received value to ETH err: ", err)
				continue
			}
			balanceEth, err := ethunits.ToEth(balance.String(), "wei")
			if err != nil {
				log.Error("Error transforming received value to ETH err: ", err)
				continue
			}
			d.Session.ChannelMessageSend(
				uChannel.ID,
				fmt.Sprintf(
					"Received %s ETH in your wallet. Total balance %s ETH",
					valueEth,
					balanceEth,
				),
			)
		}
	}

	return nil
}

func (d *Client) prefixRegexString() string {
	if d.BotUser == nil {
		log.Fatal("BotUser is nil. Did you get the right info about the bot?")
		return ""
	}

	return "<@" + d.BotUser.ID + ">"
}
