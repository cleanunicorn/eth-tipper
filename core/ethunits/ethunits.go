package ethunits

import (
	"fmt"
	"math/big"
	"strings"

	"github.com/ALTree/bigfloat"
	log "github.com/sirupsen/logrus"
)

func getCoefficientToWei(from string) (*big.Float, error) {
	var (
		c        = big.NewFloat(10).SetPrec(4096)
		fromUnit int
		toUnit   = 1
		ok       bool
	)

	units := make(map[string]int)

	units["wei"] = 1
	units["kwei"] = 2
	units["mwei"] = 3
	units["gwei"] = 4
	units["szabo"] = 5
	units["finney"] = 6
	units["ether"] = 7
	units["eth"] = 7
	units["kether"] = 8
	units["mether"] = 9
	units["gether"] = 10
	units["tether"] = 11

	if fromUnit, ok = units[strings.ToLower(from)]; ok != true {
		log.Error("Did not find unit: ", from)
		return nil, fmt.Errorf("did not find unit: %s", from)
	}

	c = bigfloat.Pow(c, big.NewFloat(float64(fromUnit-toUnit)*3))

	return c, nil
}

// ToWei transforms to wei from the specified unit
func ToWei(value string, fromUnit string) (string, error) {
	var n = new(big.Float).SetPrec(4096)

	c, err := getCoefficientToWei(fromUnit)
	if err != nil {
		return "", err
	}

	n, ok := n.SetString(value)
	if ok != true {
		return "", fmt.Errorf("Could not transform %s to float", value)
	}

	n.Mul(n, c)

	return n.Text('f', -1), nil
}

func ToEth(value string, fromUnit string) (string, error) {
	var n = new(big.Float).SetPrec(4096)

	c, err := getCoefficientToWei(fromUnit)
	if err != nil {
		return "", err
	}

	cEth, err := getCoefficientToWei("eth")
	if err != nil {
		return "", err
	}

	n, ok := n.SetString(value)
	if ok != true {
		return "", fmt.Errorf("Could not transform %s to float", value)
	}

	n.Mul(n, c)
	n.Quo(n, cEth)

	return n.Text('f', -1), nil
}
