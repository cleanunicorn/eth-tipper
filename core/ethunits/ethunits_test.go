package ethunits

import (
	"fmt"
	"testing"
)

func TestToWei(t *testing.T) {
	type args struct {
		from string
		unit string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "transform 1 eth to wei",
			args: args{
				from: "1",
				unit: "eth",
			},
			want:    "1000000000000000000",
			wantErr: false,
		},
		{
			name: "transform 1.2 eth to wei",
			args: args{
				from: "1.2",
				unit: "eth",
			},
			want:    "1200000000000000000",
			wantErr: false,
		},
		{
			name: "transform 0.002 eth to wei",
			args: args{
				from: "0.002",
				unit: "eth",
			},
			want:    "2000000000000000",
			wantErr: false,
		},
		{
			name: "transform 1 wei to wei",
			args: args{
				from: "1",
				unit: "wei",
			},
			want:    "1",
			wantErr: false,
		},
		{
			name: "transform 1 ETH to wei",
			args: args{
				from: "1",
				unit: "ETH",
			},
			want:    "1000000000000000000",
			wantErr: false,
		},
		{
			name: "cannot transform 1 unknown-unit to wei",
			args: args{
				from: "1",
				unit: "unknown-unit",
			},
			wantErr: true,
		},
		{
			name: "transform .1 finney to wei (support direct decimal format)",
			args: args{
				from: ".1",
				unit: "finney",
			},
			want:    "100000000000000",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ToWei(tt.args.from, tt.args.unit)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToWei() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ToWei() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getCoefficient(t *testing.T) {
	type args struct {
		from string
		to   string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "from ether to wei",
			args: args{
				from: "ether",
			},
			want: "1000000000000000000",
		},
		{
			name: "from eth to wei",
			args: args{
				from: "eth",
			},
			want: "1000000000000000000",
		},
		{
			name: "from kwei to wei",
			args: args{
				from: "kwei",
			},
			want: "1000",
		},
		{
			name: "from wei to wei",
			args: args{
				from: "wei",
			},
			want: "1",
		},
		{
			name: "from tether to wei",
			args: args{
				from: "tether",
				to:   "wei",
			},
			want: "1000000000000000000000000000000",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getCoefficientToWei(tt.args.from)
			if (err != nil) != tt.wantErr {
				t.Errorf("getCoefficient() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			gotString := fmt.Sprintf("%.100g", got)
			if gotString != tt.want {
				t.Errorf("getCoefficient() = %v, want %v", gotString, tt.want)
			}
		})
	}
}

func TestToEth(t *testing.T) {
	type args struct {
		value    string
		fromUnit string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "transform wei to eth",
			args: args{
				value:    "100",
				fromUnit: "wei",
			},
			want: "0.0000000000000001",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ToEth(tt.args.value, tt.args.fromUnit)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToEth() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ToEth() = %v, want %v", got, tt.want)
			}
		})
	}
}
