package monitor

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cleanunicorn/eth-tipper/core/types"
)

func scrapeBlock(block int64, nodeURL string, txChan chan<- types.Transaction) error {
	var b types.RPCBlockResponse

	bq := types.RPCBlockRequest{
		ID:      1,
		Jsonrpc: "2.0",
		Method:  "eth_getBlockByNumber",
		Params: []interface{}{
			fmt.Sprintf("0x%x", block), // Block number as hex
			true, // Also retrieve transactions
		},
	}

	bqBytes, err := json.Marshal(bq)
	blockResponse, err := doRequest(nodeURL, string(bqBytes), nil)
	if err != nil {
		log.Error("Error getting response for block ", block, " err ", err)
		return err
	}

	if err := json.Unmarshal(blockResponse, &b); err != nil {
		log.Error("Error unmarshalling response for block ", block, " err ", err)
		return err
	}

	for _, tx := range b.Result.Transactions {
		txChan <- tx
	}

	return fmt.Errorf("Only for testing")
}
