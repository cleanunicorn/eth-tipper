package monitor

import (
	"io/ioutil"
	"net/http"
	"strings"
)

func doRequest(url string, rpcRequest string, httpClient *http.Client) ([]byte, error) {
	request, err := http.NewRequest("POST", url, strings.NewReader(rpcRequest))
	if err != nil {
		return nil, err
	}
	defer request.Body.Close()

	request.Header.Add("Content-Type", "application/json")

	// If not client was specified create one now
	if httpClient == nil {
		// Define HTTP client
		var httpTransport = &http.Transport{}
		httpClient = &http.Client{Transport: httpTransport}
	}
	response, err := httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	responseBody, err := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		return nil, err
	}

	return responseBody, nil
}
