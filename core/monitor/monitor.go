package monitor

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	bolt "github.com/coreos/bbolt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cleanunicorn/eth-tipper/core/types"
)

type Client struct {
	NodeURL         string
	Database        *bolt.DB
	TransactionChan chan<- types.Transaction
}

type blockNumberStatus struct {
	Number int64 `json:"Number"`
}

var bucketScrapedBlock = []byte("lastScrapedBlock")

// Start monitor and process the blocks
func (m *Client) Start(lagBlocks int64) {
	var latestBlockNumber, previousBlockNumber int64

	// Load last previous block number
	err := ensureBuckets(m.Database)
	if err != nil {
		log.Fatalf("Could not create buckets for monitor, err: %v", err)
	}
	previousBlockNumber, err = getLastScrapedBlock(m.Database)
	if err != nil {
		log.Error("Could not get previous block number from database, err: ", err)
	}
	log.Infof("Starting from previousBlockNumber: %d", previousBlockNumber)

	// Define HTTP client
	var httpTransport = &http.Transport{}
	var httpClient = &http.Client{Transport: httpTransport}

	// Pack request
	latestBlockRequest := map[string]interface{}{
		"jsonrpc": "2.0",
		"method":  "eth_blockNumber",
		"params":  []interface{}{},
		"id":      1,
	}
	latestBlockJSON, err := json.Marshal(latestBlockRequest)
	if err != nil {
		log.Println("Error marshalling latest block number", err)
	}

	// Limit amount of consecutive errors and stop if this is reached
	errorMax := 5
	errorCount := 0

	for {
		// Get latest block
		latestBlockResponse, err := doRequest(m.NodeURL, string(latestBlockJSON), httpClient)
		if err != nil {
			log.Error("Could not get latest block", err)
			log.Error("Error count", errorCount)
			if errorCount >= errorMax {
				log.Fatal("Max error count hit, exiting")
			}
			log.Info("Sleeping for ", errorCount, "seconds")
			time.Sleep(time.Duration(errorCount) * time.Second)
			errorCount++

			continue
		} else {
			errorCount = 0
		}

		// Unpack latest block
		var latestBlock rpcGetLatestBlockNumber
		err = json.Unmarshal(latestBlockResponse, &latestBlock)
		if err != nil {
			log.Error("Error unmarshalling ", err)
		}

		// Transform the latest block number to int
		latestBlockNumber, err = strconv.ParseInt(latestBlock.Result, 0, 64)

		// Lag a few blocks to scrape consolidated blocks
		latestBlockNumber -= lagBlocks
		if latestBlockNumber < 0 {
			latestBlockNumber = 0
		}

		// If there is a new latest block make sure to add all block numbers since the last received latest block
		// Note: sometimes the reported latest block number isn't the one immediately after the previous one
		// 		ex: returned 5000, returned 5003 - make sure to add 5001, 5002 also
		if previousBlockNumber < latestBlockNumber {
			if previousBlockNumber == 0 {
				err := setLastScrapedBlock(m.Database, latestBlockNumber-1)
				if err != nil {
					log.Error("Could not set previous block number in database, err: ", err)
				}
				previousBlockNumber = latestBlockNumber - 1
			}
			for blockIterator := previousBlockNumber + 1; blockIterator <= latestBlockNumber; blockIterator++ {
				log.Debug("Scraping block ", blockIterator)
				scrapeBlock(blockIterator, m.NodeURL, m.TransactionChan)
			}
			err := setLastScrapedBlock(m.Database, latestBlockNumber)
			if err != nil {
				log.Error("Could not set previous block number in database, err: ", err)
			}
			previousBlockNumber = latestBlockNumber
		}

		// Sleep between requests
		time.Sleep(1 * time.Second)
	}

}

// getLastScrapedBlock retrieves last scraped block or 0
func getLastScrapedBlock(db *bolt.DB) (int64, error) {
	var block int64

	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketScrapedBlock)

		var bl blockNumberStatus

		err := json.Unmarshal(b.Get([]byte("blockNumber")), &bl)
		if err != nil {
			log.Error("Could not unmarshal blockNumber, err: ", err)
			return err
		}

		block = bl.Number

		return nil
	})
	if err != nil {
		return 0, err
	}

	return block, nil
}

// setLastScrapedBlock saves the latest scraped block
func setLastScrapedBlock(db *bolt.DB, blockNumber int64) error {
	err := db.Update(func(tx *bolt.Tx) error {
		var err error
		b := tx.Bucket(bucketScrapedBlock)

		var bl blockNumberStatus
		bl.Number = blockNumber
		blJSON, err := json.Marshal(bl)

		err = b.Put([]byte("blockNumber"), []byte(blJSON))
		return err
	})

	return err
}

func ensureBuckets(db *bolt.DB) error {
	err := db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists([]byte(bucketScrapedBlock))
		return nil
	})
	if err != nil {
		log.Error("Could not create buckets", err)
		return err
	}

	return nil
}

type rpcGetLatestBlockNumber struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  string `json:"result"`
	ID      int    `json:"id"`
}
