package monitor

import (
	"testing"

	"gitlab.com/cleanunicorn/eth-tipper/core/types"
)

func Test_scrapeBlock(t *testing.T) {
	type args struct {
		block   int64
		nodeURL string
		txChan  chan<- types.Transaction
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Process transactions to users wallets",
			args: args{
				block:   5674523,
				nodeURL: "http://127.0.0.1:8545",
				txChan:  make(chan<- types.Transaction),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := scrapeBlock(tt.args.block, tt.args.nodeURL, tt.args.txChan); (err != nil) != tt.wantErr {
				t.Errorf("scrapeBlock() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
