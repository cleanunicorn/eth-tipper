## Commands

### Help
`@midas help`

Displays help

```
@midas balance display your account's balance
@midas deposit create and display your account address
@midas send @username 1 eth send 1 eth to @username
@midas withdraw [0xaccountAddress] [value] eth withdraw your account's balance into an account you have access to
@midas withdraw [0xaccountAddress] withdraw full account's balance into an account you have access to
```

### Deposit and withdraw
`@midas deposit`

Display the address where the user can store funds for future tips.

Bot retrieves the user's associated address (or creates a new one if none is available). This address is unique for each user; this is how the bot knows how much was sent by each user. This is a proxy address and the funds will be moved to a centralized wallet as soon as the funds are available for moving.

`@midas withdraw {ethereum address} [value]`
- `ethereum address` an ethereum address 
- `value` how much to withdraw; omitting this will withdraw everything

Withdraw `value` ETH to the specified address

Bot creates a transaction to the specified address to send the funds to the user. The fee will be subtracted from the balance requested, meaning the user will not receive the full amount requested, but the full amount minus the fee needed for the transaction.

### Transfers
`@midas send @username {value} ETH`

- `value` number representing the number of ETH

Example: `@midas send @cleanunicorn 1 ETH`

Bot does not actually make any transaction, but updates the balances for the user who send the funds and for the user who receives the funds. An actual transaction is not created because we don't want to pay fees for each tip, only for withdraws. 

`@midas receive @username {value} {coin}`

Request amount from user.
After you request funds from that user, that person needs will be asked if that is ok and has to reply with `!tipper approve {requestID}`

Bot requests the funds from the mentioned user and waits for a confirmation. If the confirmation is positive, the bot will update the balances for the sender and receiver. Again, an actual transaction is not created.

### Balance
`@midas balance`
Display available balance

Bot reads the available balance for the user and displays the amount. This isn't exactly how much the user can receive because the fee will be subtracted out of the user's balance (21000 gas * gas price).
